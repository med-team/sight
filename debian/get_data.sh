#!/bin/bash

cd ..

if [ -d "data" ]
then
	nbfic=$(ls -R data | wc -l)
	if [ $nbfic -ne 710 ]
	then
		rm -rf ./data
		debian/get_data.sh
	else
		echo Data are now at : $PWD/data
	fi
else
	mkdir data
	cd data
	wget -O data_tmp.tar.gz "https://owncloud.u-strasbg.fr/public.php?service=files&t=cf16caedb901e9f2be2a0e21ea6c18bf&download"
	tar -xvf data_tmp.tar.gz
	rm -rf data_tmp.tar.gz
	echo Data are now at : $PWD/data
fi

